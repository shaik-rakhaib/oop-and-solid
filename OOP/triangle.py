import math
class Triangle:
    def __init__(self):
        self.points=[]
        
    def add_point(self,x,y):
        self.points.append([x,y])
        
    def perimeter(self):
        if self.check():
            ans=0
            for i in range(len(self.points)):
                x1, y1 = self.points[i]
                x2, y2 = self.points[(i+1)%3]
                ans+=math.sqrt((x2-x1)**2+(y2-y1)**2)
            return ans
        else:
            return -1
        
    def __eq__(self,tri):
        t1=sorted(self.points)
        t2=sorted(tri.points)
        if len(t1)!=len(t2):
            print("Not Equal")
            return
        for i in range(len(t1)):
            if t1[i][0]!=t2[i][0] or t1[i][1]!=t2[i][1]:
                print("Not Equal")
                return
        print("Equal")
    
    def check(self):
        if len(set([tuple(sorted(p)) for p in self.points])) != 3:
            return False
        else:
            return True

t1=Triangle()
t1.add_point(0,0)
t1.add_point(0,3)
t1.add_point(4,0)
t1_perim=(t1.perimeter())

t2=Triangle()
t2.add_point(1,2)
t2.add_point(2,1)
t2.add_point(1,5)
t2_perim=(t2.perimeter())

# t1.__eq__(t2)

t3=Triangle()
t3.add_point(1,2)
t3.add_point(2,1)
t3.add_point(1,5)

(t1 == t3)
# t1.__eq__(t3)
# t3.__eq__(t1)
