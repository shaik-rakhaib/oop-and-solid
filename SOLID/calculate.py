from data_analyser import Analyser
from data_extractor import Extractor

class Calculate:
    
    def extract_data(path):
        calculate1=Extractor(path)
        return calculate1.data
    
    def analyse_data(data):
        calculate2=Analyser(data)
        return calculate2.ans
        