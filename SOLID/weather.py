from calculate import Calculate

file_path='/home/shaik-rakhaib/Documents/OOP and SOLID/SOLID/weather.dat'

weather_obj=Calculate()

extracted_data=weather_obj.extract_data(file_path)

day=0
max_temp=1
min_temp=2
result=weather_obj.analyse_data(extracted_data,c1=day,c2=max_temp,c3=min_temp)

print(result)