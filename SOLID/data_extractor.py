class Extractor:
    
    def __init__(self,path):
        
        dat_file_path = path
        self.data = []
        with open(dat_file_path, 'r') as dat_file:
            for line in dat_file:
                columns = line.strip().split()
                self.data.append(columns)
